
console.log(window.location.search); //?courseId=6131b2f45528f7bcdda85a56
let params = new URLSearchParams(window.location.search); 
//method of URLSearchParams
	//URLSearchParams.get()
let courseId = params.get('courseId');
console.log(courseId);

let courseName = document.querySelector('#courseName');
let courseDesc = document.querySelector('#courseDesc');
let coursePrice = document.querySelector('#coursePrice');
let enrollmentContainer = document.querySelector('#enrollmentContainer');

let token = localStorage.getItem('token');

fetch(`http://localhost:3000/api/courses/${courseId}`,
	{
		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	}
)
.then( result => result.json())
.then( result => {

	// console.log(result)	//sing course document/object

	// to show details in the web page
	courseName.innerHTML = result.name
	courseDesc.innerHTML = result.description
	coursePrice.innerHTML = result.price
	enrollmentContainer.innerHTML =
	`
		<button id="enrollButton"class="btn btn-primary btn-block">Enroll</button>
	`

	// to make enroll button work
	let enrollButton = document.querySelector('#enrollButton');

	enrollButton.addEventListener("click", () => {

		fetch(`http://localhost:3000/api/users/enroll`,
		{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
					courseId: courseId /*or result._id*/	
			})
		}
		)
		.then (result => result.json())
	}) 	.then (result => {
			console.log(result)
			
			if(result) {
				alert("Enroll Successfully")

				window.location.replace('./course')
			} else {
				alert('Something went wrong')
			}

	})

})